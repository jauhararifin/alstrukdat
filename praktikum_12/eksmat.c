/*
 * Author
 * Nama : Jauhar Arifin
 * NIM : 13515049
 * File : eksmat.c
 * Tanggal : 17 November 2016
 */

#include "bintree.h"

#include <stdio.h>
#include <string.h>

/*
 * buildTree
 * digunakan untuk menciptakan pohon evaluasi berdasarkan string exp
   dan menghasilkan panjang string yang dievalusi.
 * char* exp : string yang akan dievaluasi
 * int* length : panjang string yang berhasil di evaluasi
 * return : BinTree yang merupakan pohon evaluasi
 */
BinTree buildTree(char* exp, int *length) {	
	if (*exp >= '0' && *exp <= '9')
		return Tree((int) *exp - '0', Nil, Nil);
	else {
		BinTree v1 ,v2;
		
		*length = 1;
		int l2;
		if (exp[*length] >= '0' && exp[*length] <= '9') {
			v1 = Tree((int) exp[*length]-'0', Nil, Nil);
			(*length)++;
		} else {
			v1 = buildTree(exp+(*length), &l2);
			*length += l2;
		}
		
		if (exp[*length] >= '0' && exp[*length] <= '9') {
			v2 = Tree((int) exp[*length]-'0', Nil, Nil);
			(*length)++;
		} else {
			v2 = buildTree(exp+(*length), &l2);
			*length += l2;
		}
		
		switch (*exp) {
			case '-' : return Tree(-1,v1,v2);
			case '+' : return Tree(-2,v1,v2);
			case '*' : return Tree(-3,v1,v2);
			case '/' : return Tree(-4,v1,v2);
			case '^' : return Tree(-5,v1,v2);
			default  : return Tree(-6,v1,v2);
		}
	}
}

/*
 * print_postfix
 * digunakan untuk mencetak pohon evaluasi dalam bentuk postfix
 * BinTree tree : pohon yang akan dicetak
 */
void print_postfix(BinTree tree) {
	if (IsTreeOneElmt(tree))
		printf("%d", Akar(tree));
	else {
		print_postfix(Left(tree));
		print_postfix(Right(tree));
		switch (Akar(tree)) {
			case -1 : printf("-"); break;
			case -2 : printf("+"); break;
			case -3 : printf("*"); break;
			case -4 : printf("/"); break;
			case -5 : printf("^"); break;
			case -6 : printf("%%"); break;
		}
	}
} 

/*
 * pangkat
 * menghasilkan nilai dari a^b
 * int a : basis dari bilangan yang akan dipangkatkan
 * int b : nilai eksponen
 * return : int yang merupakan hasil a^b
 */
int pangkat(int a, int b) {
	if (b == 0)
		return 1;
	int c = pangkat(a, b/2);
	if (b % 2)
		return c * c * a;
	else
		return c * c;
}

/*
 * eval_tree
 * digunakan untuk menghitung nilai dari ekspresi yang dinyatakan dalam
 * pohon evaluasi
 * BinTree tree : pohon evaluasi yang akan dihitung nilainya
 * return : int yang merupakah hasil evaluasi pohon
 */
int eval_tree(BinTree tree) {
	if (IsTreeOneElmt(tree))
		return Akar(tree);
	else {
		switch (Akar(tree)) {
			case -1 : return eval_tree(Left(tree)) - eval_tree(Right(tree));
			case -2 : return eval_tree(Left(tree)) + eval_tree(Right(tree));
			case -3 : return eval_tree(Left(tree)) * eval_tree(Right(tree));
			case -4 : return eval_tree(Left(tree)) / eval_tree(Right(tree));
			case -5 : return pangkat(eval_tree(Left(tree)), eval_tree(Right(tree)));
			case -6 : return eval_tree(Left(tree)) % eval_tree(Right(tree));
		}
	}
}

int main() {
	char exp[256];
	
	// Membaca input hingga endline atau EOF
	int i = 0;
	while ((exp[i] = (char) getchar()) != EOF && exp[i] != '\n' && exp[i] != '\r')
		i++;
	exp[i] = 0;
	
	// Kasus untuk ekspresi kosong
	if (strlen(exp) == 0)
		printf("Ekspresi kosong\n");
	else {
		// membentuk pohon evaluasi
		int dummy;
		BinTree tree = buildTree(exp, &dummy);
		
		// mencetak pohon evaluasi dalam bentuk postfix
		print_postfix(tree);
		
		// mencetak hasil evaluasi
		printf("\n=%d\n", eval_tree(tree));		
	}
	 
	return 0;
}
